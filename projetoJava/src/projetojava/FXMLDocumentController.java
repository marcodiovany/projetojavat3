/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetojava;

import projetojava.conexao.Conexao;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextArea interessacao;
    @FXML
    private TextField nometbx;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void interessar(ActionEvent event) {
        Conexao c = new Conexao();
        Connection conexao = c.getConexao();
        
        PreparedStatement ps;
        Statement st;
        try
        {
            String generatedColumns[] = {"id"};
            ps = conexao.prepareStatement("Insert into OO_INTERECE (interece,usuario,id) values (?,?,id_seq.nextval)",generatedColumns);
            st = conexao.createStatement();
            ps.setString(1, this.interessacao.getText());
            ps.setString(2, this.nometbx.getText());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            while(rs.next())
            {
                System.out.println("ID:"+ rs.getInt(1));
            }
        }catch(SQLException e)
        {
            System.out.println("Eu explodi");
            e.printStackTrace();
        }
        interessacao.setText("Interece Cadastrado");
    }
    
}
